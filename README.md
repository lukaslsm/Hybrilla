**This project is deprecated in favour of my new [minecraft-hybrid.py](../../../../minecraft-hybrid.py)!**

# Hybrilla [256px] [Add-On] [1.7]
Hybrilla (Hybrid-Vanilla) is a Minecraft Resource Pack designed as Add-On for xMrVizzy's Faithful 32 Pack (https://minecraft.curseforge.com/projects/236821).
To use this resource pack, you need to install the Faithful 32 pack first. All textures in this Resource Pack are based on xMrVizzy's Faithful 32 pack.

This Minecraft Resource Pack is currently WIP (Work-In-Progress). It does currently not support all features of the Vanilla Minecraft game. This Resource pack supports various mods. The mod support part is based on the F32 texture Pack (http://f32.me).

The goal of this resource pack is to combine both Low-Res and High-Res textures (256px) to be able to play Minecraft with HD textures while keeping the "Vanilla-Feeling". To play this pack, you need a beefy computer with at least 8GB RAM (recommended) and a good graphics card. This pack is not intended to replace all in-game textures, but to replace as much as necessary. If you miss a block or item, feel free to create an issue here.

### Terms of Use
You need to follow the Terms of Use specified here: https://minecraft.curseforge.com/projects/236821 .

If you want to create a changed version of Faithful or Hybrilla, you must follow the rule that your Resource Pack should be like an add-on for Faithful, which means that you place only textures created by yourself in the modified add-on resource pack and link back to this page and the page of the Faithful pack (https://minecraft.curseforge.com/projects/236821).

### How to install this pack
You need to download this Pack and the Faithful 32 pack into the resourcepacks/ folder of your minecraft directory. To download this repository as Resource Pack, you can click on "Clone or Download"->"Download ZIP" in the top right cornder of the file browser above.
In Minecraft, you need to install both the Faithful Pack and the Hybrilla Pack by placing the Hybrilla Pack over the faithful pack. This way, every texture that is not contained in the Hybrilla pack is being replaced by the original Faithful32 texture.

### Screenshots
![Screenshot1](/screenshots/mcHybrilla-Thumb1.png?raw=true "Screenshot 1")

### Changelog
* 10.08.2017 Upload of Mod Textures.
* 09.08.2017 Initial Upload, basic support for the Vanilla Game. I could not upload the paintings texture due to copyright issues, sorry.
